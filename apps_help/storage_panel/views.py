from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect

class IndexView(TemplateView):
    template_name = 'apps_help/storage_panel/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context
