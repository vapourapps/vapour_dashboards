from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard

class Backup(horizon.Panel):
    name = _("Backup")
    slug = "backup"


dashboard.Apps_Help.register(Backup)
