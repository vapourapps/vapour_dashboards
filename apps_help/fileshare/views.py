from horizon import views
from openstack_dashboard.api import nova


class IndexView(views.APIView):
    # A very simple class-based view...
    template_name = 'apps_help/fileshare/index.html'

    def get_data(self, request, context, *args, **kwargs):
        instances_list, more = nova.server_list(self.request)
        if 'va-fileshare' in [x.name for x in instances_list]: 
            fileshare_instance = [x for x in instances_list if x.name == 'va-fileshare'][0]
            print fileshare_instance
            ip_type = 'private'
            if 'vmnet' in fileshare_instance.addresses:
                ip_type = 'vmnet'
            context['fileshare_ip'] = fileshare_instance.addresses.get(ip_type)[0].get('addr')

       # Add data to the context here...
        return context
