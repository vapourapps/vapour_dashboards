from horizon import views
from openstack_dashboard.api import nova


class IndexView(views.APIView):
    # A very simple class-based view...
    template_name = 'apps_help/vap_email/index.html'

    def get_data(self, request, context, *args, **kwargs):
        instances_list, more = nova.server_list(self.request)
        print [x.name for x in instances_list]
        if 'va-email' in [x.name for x in instances_list]: 
            email_instance = [x for x in instances_list if x.name == 'va-email'][0]
            ip_type = email_instance.addresses.keys()[0]
            context['email_ip'] = email_instance.addresses.get(ip_type)[0].get('addr')

        # Add data to the context here...
        return context
