from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

class Shares(tables.DataTable):
    shared_folder = tables.Column('shared_folder', verbose_name=_("Shared folder"))
    users_with_access = tables.Column('users_with_access', verbose_name = _('Users with access'))
    last_access = tables.Column('last_access', verbose_name=_("Last access"))
    class Meta:
        name = "Shares"
        verbose_name = _("Shares")

class Users(tables.DataTable):
    name = tables.Column('name', verbose_name=_("Name"))
    quota = tables.Column('quota', verbose_name=_("Quota"))
    last_access = tables.Column('last_access', verbose_name=_("Last access"))
    class Meta:
        name = "revoked_users"
        verbose_name = _("Revoked Users")

