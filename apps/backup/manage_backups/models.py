from django.db import models

class SaltHost(models.Model):
    host_name = models.CharField(max_length=30)
    fqdn = models.CharField(max_length=30)
