from horizon import forms, messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from openstack_dashboard.salt_utils import salt_utils

directory_minion = 'G@role:directory'

class AddBackupForm(forms.SelfHandlingForm):
    backup = forms.RegexField(label=_("Backup path"), max_length=32, regex=r'[^\n]+$')
    host = ''

    def __init__(self, request, *args, **kwargs):
        super(AddBackupForm, self).__init__(self, *args, **kwargs)
#        print self.request

    def handle(self, request, data):
        result = salt_utils.add_new_backup(request, request.session['host'], data['backup'])
        if result: messages.success(request, 'Backup is added successfuly. ')
        else: messages.error(request, 'Backup wasn\'t added; Are you sure you tried to add a file that exists?')
        return redirect('horizon:apps:backup.manage_backups:index')

class KeepBackupsForm(forms.SelfHandlingForm):
    keep_backups = forms.RegexField(label=_("No. days to keep"), max_length=3, regex=r'[\d]+')

    def handle(self, request, data):
        result = salt_utils.keep_backups(request, data['keep_backups'])
        if result: messages.success(request, 'Success. ')
        else: messages.error(request, 'No changes were made. Try again or contact an administrator. ')
        return redirect('horizon:apps:backup.manage_backups:index')
