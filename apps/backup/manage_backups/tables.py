from horizon import tables
from django.core.urlresolvers import reverse, reverse_lazy
from django.utils.translation import ugettext as _

class AddBackup(tables.LinkAction):
    name = "add_backup"
    verbose_name = _("Add Backup")
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True

    def __init__(self, **kwargs):
        context = super(AddBackup, self).__init__(**kwargs)
        return context


    def get_link_url(self):
        url = reverse_lazy("horizon:apps:backup.manage_backups:new_backup", args = [''])
        return url

class KeepBackups(tables.LinkAction):
    name = "keep_backups"
    verbose_name = _("Keep backups")
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True
    def get_link_url(self):
        url = reverse_lazy("horizon:apps:backup.manage_backups:keep_backup", args = [''])
        return url


class RemoveBackup(tables.LinkAction):
    name = "remove_backup"
    verbose_name = _("Remove Backup")
    def get_link_url(self, backup):
        url = reverse_lazy("horizon:apps:backup.manage_backups:remove_backup", args = ['', backup.backup_name])
        return url


class BackupTable(tables.DataTable):
    backup_name = tables.Column('backup_name', verbose_name = _('Backup name: '))
    host_info = {}
    class Meta:
        name = 'instances_table'
        verbose_name = _("Instance backups")
        #Keep backups was used to modify how long backups should be kept for. Not used right now. 
#        table_actions = (AddBackup, KeepBackups, )
        table_actions = (AddBackup, )
        row_actions = (RemoveBackup, )

    def __init__(self, *args, **kwargs):
        self.host_info = kwargs.pop('host_info')
        super(BackupTable, self).__init__(*args, **kwargs)

    def render_backup_name(self):
        print 'Im here'
        return self.host_info['name'] + ' (last backup : ' + self.host_info['last_backup'] + ')'
