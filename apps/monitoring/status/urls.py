from django.conf.urls import patterns
from django.conf.urls import url
from horizon import tables
import subprocess, json
from ...utils import *
from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^chartr/$', views.chart_rest, name='chartr'),
    url(r'^chartr/(?P<host>.+):(?P<service>.+):(?P<start>.+):(?P<interval>.+)$', views.chart_rest, name='chartr'),
    url(r'^chart/(?P<host>.+):(?P<service>.+)$', views.ChartView.as_view(), name='chart'),
)
