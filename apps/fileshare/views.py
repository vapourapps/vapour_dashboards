from django.views.generic import TemplateView
import tables as project_tables
from salt_utils import salt_utils

class FileShare(object):
    def __init__(self, folder_name = '', used_space = '0'):
        self.id = self.folder_name = folder_name
        self.used_space = used_space + 'M' if used_space != '1' else '<1M'

class IndexView(TemplateView):
    template_name = 'apps/fileshare/index.html'
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        result = salt_utils.get_fileshares(self.request)
        print 'Result is : ', result
        home_fileshares = project_tables.SharedFolders(self.request, [FileShare(folder_name = x['folder'], used_space = x['bytes']) for x in result['home'] if x['folder'] and x['folder'][-1] != '_'])
        share_folder = project_tables.SharedFolders(self.request, [FileShare(folder_name = x['folder'], used_space = x['bytes']) for x in result['share']])
        public_folder = project_tables.SharedFolders(self.request, [FileShare(folder_name = x['folder'], used_space = x['bytes']) for x in result['public']])

        context['home_table'] = home_fileshares
        context['share_table'] = share_folder
        context['public_table'] = public_folder
        return context
