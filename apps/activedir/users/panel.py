import horizon
from ... import dashboard

class UserPanel(horizon.Panel):
    name = "Users"
    slug = "activedir.users"

dashboard.Dashboard.register(UserPanel)
