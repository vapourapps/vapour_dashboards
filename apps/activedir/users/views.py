from horizon import tables, forms, messages
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from openstack_dashboard.salt_utils.utils import *
from openstack_dashboard.salt_utils import salt_utils
from . import forms as project_forms
from . import tables as project_tables

directory_minion = 'G@role:directory'

def sync_cache(request):
    salt_utils.sync_cache(request)
    return redirect('horizon:apps:activedir.users:index')

class IndexView(tables.DataTableView):
    table_class = project_tables.UsersTable
    template_name = 'apps/activedir.users/table.html'
    filtered_users = ['krbtgt', 'dnsquery', 'query']

    def get_data(self):
        search_field = 'usertable__filter_users__q'
        is_search = (self.request.method == 'POST' and 
            self.request.POST.get(search_field, None) is not None)
        number_of_users = 30
        last_user = self.request.GET.get('users_marker', '')
        
        result = salt_utils.get_samba_users(self.request)
        result['users'] = sorted(result['users'], key=lambda i: i['username'].lower())
        last_user = [x for x in result['users'] if x['username'] == last_user] or [None]
        last_user = last_user[0]
        if last_user: last_user_index = result['users'].index(last_user) + 1
        else: last_user_index = 0

        if not is_search:
            # If we are not searching, filter the page so that it has `number_of_users` per page
            result['users'] = result['users'][last_user_index:last_user_index + number_of_users]

        self.request.session.set_test_cookie()

        users_result = result['users']
        groups = result['groups']
        if groups : 
            self.request.session['samba_groups_with_users'] = groups
            
        samba_log = result['samba_log']
        users = []
        for user in users_result:
            if user['username'] in self.filtered_users : 
                continue
            log_user = [x for x in samba_log if x['info']['username'] == user['username']]
            if log_user : log_user = log_user[0]
            else : log_user = {'date' : '','info': { 'computer' : '', 'address' : ''}}
            new_user = User(user["username"], user["name"], user["status"],
                              '', log_user["date"],
                             log_user['info']["address"])
            users.append(new_user)


        print 'Users are : ', [x.status for x in users]

        if is_search: # If it's a search, filter based on username field
	    filtered_users = []
	    search_lowcase = self.request.POST.get(search_field, None).lower()
	    for user in users:
                if search_lowcase in user.username.lower():
                    filtered_users.append(user)  
            return filtered_users
        return users


    #    def get_context_data(self, **kwargs):
    #        context = super(IndexView, self).get_context_data(**kwargs)
    #        context['table'].paginate(page = request.GET.get('page', 1), per_page = 25)
    #        print 'Table now is : ', table
    #        return context


    def get_context(self):
        context = super(IndexView, self).get_context()

        table = context['table']
        context['table'] = context['table'].paginate(page = request.GET.get('page', 1), per_page = 25)
        return context

class AddUserView(forms.ModalFormView):
    form_class = project_forms.AddUserForm
    template_name = "apps/activedir.users/add.html"
    success_url = reverse_lazy('horizon:apps:activedir.users:index')

    def get_context_data(self, **kwargs):
        context = super(AddUserView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {}

    def get_form_kwargs(self):
        kwargs = super(AddUserView, self).get_form_kwargs()
        organizational_units = salt_utils.list_organizational_units(self.request)
        kwargs['organizational_units'] = organizational_units 
        return kwargs
