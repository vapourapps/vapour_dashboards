from horizon import forms, messages
from django.core.urlresolvers import reverse_lazy
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _
#from ...utils import *
from openstack_dashboard.salt_utils import salt_utils

directory_minion = 'G@role:directory'

class AddUserForm(forms.SelfHandlingForm):
    username = forms.RegexField(label=_("Username"), max_length=32, regex=r'[^\n]+$')
    password = forms.CharField(widget=forms.PasswordInput)
    name = forms.CharField(label=_("First name"), max_length=64)
    surname = forms.CharField(label=_("Last name"), max_length=64)
    email = forms.RegexField(label=_("E-mail"), max_length = 64, regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
    organizational_unit = forms.ChoiceField(label=_('Organizational unit'), choices = [(None, '-')])
    add_axigen = forms.BooleanField(label=_("Enable Axigen"), initial=False, required = False)
    change_at_next_login = forms.BooleanField(label=_("Change password at next login"), initial = False, required = False)

    def __init__(self, request, **kwargs):
        organizational_units = kwargs.pop('organizational_units')
        super(AddUserForm, self).__init__(kwargs)

        self.fields['organizational_unit'].choices += [(x[0], x[0]) for x in organizational_units]

        if kwargs.get('data') is not None: 
            data = kwargs.get('data') 
            print(data)
            axi_is_enabled = (data.get('add_axigen') is not None)
            data = {x : data.get(x) for x in ['username', 'password', 'name', 'surname', 'email', 'organizational_unit', 'change_at_next_login']}
            data['vpn'] = False
            data['attrs'] = {'axi_is_enabled': axi_is_enabled}
            print 'Now it is : ', data
            self.handle(request, data)

    def handle(self, request, data):
        result = salt_utils.add_activedir_user(request, data)
        print 'Adding user result : ', result
        if result:
            messages.success(request, _("Added new user successfully!"))
            return True
        else:
            messages.error(request, _("Failed adding user:%s" % result))
            return False
