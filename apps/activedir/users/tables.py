from horizon import tables, messages
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from openstack_dashboard.salt_utils.utils import *
from openstack_dashboard.salt_utils import salt_utils
directory_minion = 'G@role:directory'

#
# Table Actions
#


class FilterUsers(tables.FilterAction):
    name = "filter_users"
#    def filter(self, table, users, filter_string):
#        users += salt_utils.get_samba_users(self.request)['users']
#        print ('Users are : ', users)
#        q = filter_string.lower()
#
##        def comp(flavor):
##            return q in users.name.lower()
##            return q in users.name.lower()
#
#        users = filter(lambda u: q in u['username'], users)
#        users = [User(user["username"], user["name"], user["is_locked"],user["password_expiry"], log_user["date"], log_user['info']["computer"], log_user['info']["address"]) for user in users]
# #       return filter(comp, users)
#
    filter_type = 'server'
    filter_choices = (
        ('username', 'Username', True),
    )

class AddUser(tables.LinkAction):
    name = "add_user"
    verbose_name = _("Add user")
    url = "horizon:apps:activedir.users:add"
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True


class DeleteUser(tables.DeleteAction):
    name = "deleterule"

    def action_present(self, count):
        return _("Delete users") if count > 1 else _("Delete user")

    def action_past(self, count):
        return _("Deleted users") if count > 1 else _("Deleted user")

    def delete(self, request, obj_id):
        result = salt_utils.delete_activedir_user(request, obj_id)
        result = salt_utils.revoke_openvpn_user(request, obj_id)
        return result


class ChangeOU(tables.LinkAction):
    name = "change_ou"
    verbose_name = _("Change organizational unit")
    url = "horizon:apps:activedir.users:change_ou"
    classes = ("ajax-modal", "btn-edit")

class ChangeName(tables.LinkAction):
    name = "change_name"
    verbose_name = _("Change name")
    url = "horizon:apps:activedir.users:change_name"
    classes = ("ajax-modal", "btn-edit")

class EditUser(tables.LinkAction):
    name = "edit_user"
    verbose_name = _("Edit user")
    url = "horizon:apps:activedir.users:edit_user"
    classes = ('ajax-modal', 'btn-edit')

class ChangePassword(tables.LinkAction):
    name = "change_password"
    verbose_name = _("Change password")
    url = "horizon:apps:activedir.users:change_password"
    classes = ("ajax-modal", "btn-edit")

class LockUser(tables.DeleteAction):
    name = "lock_user"

    def action_present(self, count):
        return _("Lock users") if count > 1 else _("Lock user")

    def action_past(self, count):
        return _("Locked users") if count > 1 else _("Locked user")

    def delete(self, request, obj_id):
        return True 

class UnlockUser(tables.DeleteAction):
    name = "unlock_user"

    def action_present(self, count):
        return _("Unlock users") if count > 1 else _("Unlock user")

    def action_past(self, count):
        return _("Unlocked users") if count > 1 else _("Unlocked user")

    def delete(self, request, obj_id):
        result = salt_utils.unlock_samba_user(request, obj_id)
        print 'Unlock is : ', result
        return True 

class DisableUser(tables.DeleteAction):
    name = "disable_user"

    def action_present(self, count):
        return _("Disable users") if count > 1 else _("Disable user")

    def action_past(self, count):
        return _("Disabled users") if count > 1 else _("Disabled user")

    def delete(self, request, obj_id):
        api = get_salt_api(request)
        result = api.call_function_single_target(directory_minion, 'samba.set_user_status', {
            'lock': True,
            'username': obj_id
        })
        return result is None

class EnableUser(tables.DeleteAction):
    name = "enable_user"
    verbose_name = _("Enable")

    def action_present(self, count):
        return _("Enable users") if count > 1 else _("Enable user")

    def action_past(self, count):
        return _("Enabled users") if count > 1 else _("Enabled user")

    def delete(self, request, obj_id):
        api = get_salt_api(request)
        result = api.call_function_single_target(directory_minion, 'samba.set_user_status', {
            'lock': False,
            'username': obj_id
        })
        return result is None

class ListLogins(tables.LinkAction):
    name = "list_logins"
    verbose_name = _("List logins")
    url = "horizon:apps:activedir.users:list_logins"
    classes = ("btn-edit", )


class ManageGroups(tables.LinkAction):
    name = "manage_groups"
    verbose_name = _("Manage groups")
    url = "horizon:apps:activedir.users:manage_groups"
    classes = ("ajax-modal", "btn-edit")

class SyncCache(tables.LinkAction):
    name = "sync_cache"
    verbose_name = _("Synchronize cache")
    url = "horizon:apps:activedir.users:sync_cache"

#
# Actual tables
#


class UsersTable(tables.DataTable):
    username = tables.Column('username', verbose_name=_("Username"),
                             link='horizon:apps:activedir.users:list_logins')
    name = tables.Column('name', verbose_name=_("Name"))
    status = tables.Column('status', verbose_name=_("Status"))
    expiry = tables.Column('expiry', verbose_name=_("Description"))
    lastLogon = tables.Column('lastLogon', verbose_name=_("Last login"))
    ip = tables.Column('ip', verbose_name=_("IP address"))
    status = tables.Column('status', verbose_name = _("Status"), status = True, status_choices = (
                            (_("OK"), True),
                            (_("Disabled"), False), 
                            (_("Locked"), False), 
                            (_("Locked, Disabled"), False)
                            )
    )
#    status = tables.Column('status', status=True, status_choices=(
#                           (_("OK"), True), (_("User locked"), False), (_("Password expiry soon"), False), (_("Password is expired"), False)
#                           )
#    )

    def get_object_display(self, object):
        return object.username


    def has_more_data(self):
        return True

    class Meta:
        name = "usertable"
        verbose_name = _("Users")
        table_actions = (FilterUsers, AddUser, SyncCache)
        row_actions = (DeleteUser, EditUser, ChangePassword, DisableUser, EnableUser, UnlockUser, ListLogins, ManageGroups, ChangeOU)
        status_columns = ("status", )
        pagination_param = 'users_marker'
        prev_pagination_param = 'prev_users_marker'
