from horizon import tables, forms, messages
from django.core.urlresolvers import reverse_lazy
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import redirect
from ...utils import *
from openstack_dashboard.salt_utils import salt_utils
import urllib, socket

directory_minion='G@role:directory'


class DNSEntry(object):
    def __init__(self, group_name, entry_type, value):
        self.id = '%s:%s:%s' % (group_name, entry_type, value)
        self.group_name = group_name
        self.entry_type = entry_type
        self.value = value

class FilterDNS(tables.FilterAction):
    name = "filterDNS"
    def filter(self, table, groups, filter_string):
        q = filter_string.lower()

        def comp(flavor):
            return q in groups.ip.lower()

        return filter(comp, users)

class AddAction(tables.LinkAction):
    name = "add"
    verbose_name = _("Add DNS entry")
    url = "horizon:apps:activedir.dns:add"
    classes = ("btn-launch", "ajax-modal")
    ajax = True

class EditDNS(tables.LinkAction):
    name = "edit"
    verbose_name = _("Edit DNS entry")
    classes = ("btn-launch", "ajax-modal")
    ajax = True

    def get_link_url(self, data):
        if data.group_name == '(same as parent)' : data.group_name = ''
        if data.entry_type == 'SOA':
            data.value = '&' + data.value.replace(', ', '&')
        elif data.entry_type in ['A', 'AAAA']:
            data.value = '&address=' + data.value
        elif data.entry_type == 'MX':
            data.value = data.value.split(' ')
            data.value = '&hostname=' + data.value[0] + '&priority=' + data.value[1][1:-1]
        else:
            data.value = '&hostname=' + data.value
        return reverse_lazy("horizon:apps:activedir.dns:edit") + '?entry_type=' + data.entry_type + '&name='+data.group_name + data.value

class DeleteDNS(tables.DeleteAction):
    name = "delete_dns"

    def action_present(self, count):
        return _("Delete entries") if count > 1 else _("Delete entry")

    def action_past(self, count):
        return _("Deleted entries") if count > 1 else _("Deleted entry")

    def delete(self, request, obj_id):
        name, entry_type, data = obj_id.split(':')
        if name == '(same as parent)' : name = '@'
        if entry_type == 'SOA' : 
            return False
        if entry_type == 'MX':
            data = data.split(' ')
            data = {'name' : name, 'entry_type' : entry_type, 'hostname' : data[0].split('.')[0], 'priority' : data[1][1:-1]}
        else:
            data = {'name' : name, 'entry_type' : entry_type, 'hostname' : data, 'address' : data}
        result = salt_utils.manage_dns_entry(request, data, action = 'delete')
        return result


class DNSTable(tables.DataTable):
    group_name = tables.Column('group_name', verbose_name=_("Group name"))
    entry_type = tables.Column('entry_type', verbose_name=_("Type"))
    value = tables.Column('value', verbose_name=_("Value"))

    class Meta:
        name = "grouptable"
        verbose_name = _("DNS")
        table_actions = (FilterDNS, AddAction)
        row_actions = (DeleteDNS, EditDNS )

class DNSForm(forms.SelfHandlingForm):
    name = forms.CharField(label=_('Entry name'), max_length=128, required = False)
    entry_type = forms.ChoiceField(label=_('Type'), choices = [(x,x) for x in ['A', 'AAAA', 'MX', 'NS', 'CNAME']], required = False)
    address = forms.CharField(label=_("Address"), max_length=128, help_text = _('For type A, use IPv4, for AAAA use IPv6. '), required = False)
    hostname = forms.RegexField(regex = '[a-zA-Z0-9_\.]*', label=_("Host name"), max_length=128, required = False)
    priority = forms.CharField(label=_("Priority"), max_length=128, required = False)

    def clean(self):
        super(DNSForm, self).clean()
        if self.cleaned_data.get('address') :
            try:
              socket.inet_aton(self.cleaned_data['address'])
            except socket.error:
              raise exceptions.ValidationError('Invalid address')
        return self.cleaned_data   

    def handle(self, request, data):
        result = salt_utils.manage_dns_entry(request, data)
        if result: messages.success(self.request, 'DNS added successfuly')
        else: messages.error(self.request, 'DNS wasn\'t added; try again or contact your administrator. ')
        return result

class EditDNSForm(forms.SelfHandlingForm):
    name = forms.CharField(label=_('Name'), max_length = 50, required = False, widget = forms.TextInput(attrs={'readonly':True}))
    entry_type = forms.CharField(label=_('Entry type'), max_length = 50, required = False, widget = forms.TextInput(attrs={'readonly':True}))
    entry_fields = {}

    def __init__(self, *args, **kwargs):
        self.entry_fields = kwargs.pop('get_data')
        super(EditDNSForm, self).__init__(self, kwargs, args)
        for field in self.entry_fields:
            if field in self.fields : 
                #We use widgets to set input default values because for some reason, the initial argument
                #doesn't work when we override the __init__ method. 
                self.fields[field].widget.attrs['value'] = self.entry_fields[field]
                continue
            if self.entry_fields[field] == ('(same as parent)') : self.entry_fields[field] = ''
            self.fields[field] = forms.CharField(label=_(field), max_length = 50, widget = forms.TextInput(attrs={'value' : self.entry_fields[field]}), required = False)

    def form_valid(self, form):
        return super(ContactView, self).form_valid(form)
        

    def handle(self, request, data):
        result = salt_utils.update_dns_entry(request, self.entry_fields, request.POST)
        if self.is_valid(): pass
        if result: messages.success(request, 'DNS updated successfuly')
        else: messages.error(request, 'DNS wasn\'t updated; try again or contact your administrator. ')
        return result 


class AddView(forms.ModalFormView):
    form_class = DNSForm
    template_name = "apps/activedir.dns/add.html"
    success_url = reverse_lazy('horizon:apps:activedir.dns:index')

    def get_context_data(self, **kwargs):
        context = super(AddView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {}


class EditView(forms.ModalFormView):
    form_class = EditDNSForm
    template_name = "apps/activedir.dns/edit.html"
    success_url = reverse_lazy('horizon:apps:activedir.dns:index')

    def get_form_kwargs(self):
        kwargs = super(EditView, self).get_form_kwargs()
        kwargs['get_data'] = self.request.GET
        return kwargs

    def get_context_data(self, **kwargs):
        if self.request.GET['entry_type'] == 'SOA':
            return messages.info(self.request, 'You cannot edit SOA entries. ')
        context = super(EditView, self).get_context_data(**kwargs)
        if self.request.GET: context['get_data'] = urllib.urlencode(self.request.GET)
        context['entry_type'] = 'SOA'
        context['redirect_index'] = self.success_url
        return context

    def form_is_valid(self, data):
        for field in data:
            if not data[field]:
                if field != 'name':
                    return False
        return True



class IndexView(tables.DataTableView):
    table_class = DNSTable
    template_name = 'apps/activedir.dns/table.html'
    def get_data(self):
#        if salt_utils.get_instance_status(self.request, 'va-directory') != 'running' :
#            return [] 

        api = get_salt_api(self.request)
        try:
            result = api.call_function_single_target(directory_minion,
                                                     'samba.list_dns')
            if not isinstance(result, list):
                msg = "Didn't receive list from ActiveDirectory, " + \
                      "instead got: %r" % result
                raise exceptions.NotAvailable(msg)
        except Exception as e:
            import traceback
            traceback.print_exc()
            result = []
#            result = []
#            exceptions.raise(self.request, _("Unable to retrieve DNS entries."))

        dns_entries = []
        for entry in result:
            dns_entries.append(DNSEntry(
                group_name=entry['group_name'] if entry['group_name'] else '(same as parent)',
                entry_type=entry['type'],
                value=entry['value']
            ))
        return dns_entries
