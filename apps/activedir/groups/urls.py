from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa

from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<name>[^/]+)/members$', views.GroupMembersView.as_view(), name='view_members'),
    url(r'^add_group$', views.AddGroupView.as_view(), name='add'),
    url(r'^add_user_to_group$', views.AddUserToGroupView.as_view(), name='add_user_to_group'),
    url(r'^remove_user_from_group/(.*)$', views.RemoveUserFromGroup, name='remove_user_from_group'),
    url(r'^delete_group/(.*)$', views.DeleteGroup, name='delete_group'),
)
