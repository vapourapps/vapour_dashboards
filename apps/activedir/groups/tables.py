from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

class FilterGroups(tables.FilterAction):
    name = "filterGroups"
    def filter(self, table, groups, filter_string):
        q = filter_string.lower()

        def comp(flavor):
            return q in groups.name.lower()

        return filter(comp, users)


class ListMembers(tables.LinkAction):
    name = "list_members"
    verbose_name = _("List members")
    url = "horizon:apps:activedir.groups:view_members"
    classes = ("btn-edit", )

class AddGroup(tables.LinkAction):
    name = "add_group"
    verbose_name = _("Add group")
    url = "horizon:apps:activedir.groups:add"
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True

class AddUserToGroup(tables.LinkAction):
    name = "add_user_to_group"
    verbose_name = _("Add users to group")
    url = "horizon:apps:activedir.groups:add_user_to_group"
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True

class RemoveUserFromGroup(tables.LinkAction):
    name = "remove_user_from_group"
    verbose_name = _("Remove")
    url = "horizon:apps:activedir.groups:remove_user_from_group"

class DeleteGroup(tables.LinkAction):
    name = "delete_group"
    verbose_name = _("Delete group")
    url = "horizon:apps:activedir.groups:delete_group"

class GroupTable(tables.DataTable):
    name = tables.Column('name', verbose_name=_("Name"))
    description = tables.Column('description', verbose_name=_("Description"))
    email = tables.Column('email', verbose_name=_("E-mail"))

    class Meta:
        name = "grouptable"
        verbose_name = _("Groups")
        table_actions = (FilterGroups, AddGroup)
        row_actions = (ListMembers, DeleteGroup, )

class GroupMembersTable(tables.DataTable):
    username = tables.Column('username', verbose_name=_("Username"))
    join_date = tables.Column('join_date', verbose_name=_("Join date"))
    class Meta:
        name = 'userstable'
        verbose_name = _("Users in group")
        table_actions = (AddUserToGroup, )
        row_actions = (RemoveUserFromGroup, )
