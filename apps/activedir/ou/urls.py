from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa

from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^add_ou$', views.AddOUView.as_view(), name='add'),
    url(r'^(?P<name>[^/]+)/members$', views.OUMembersView.as_view(), name='view_members'),
#    url(r'^delete_group/(.*)$', views.DeleteGroup, name='delete_group'),
)
