from horizon import forms, messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.translation import ugettext_lazy as _
from ...utils import *
from openstack_dashboard.salt_utils import salt_utils

class AddOUForm(forms.SelfHandlingForm):
    ou_name = forms.RegexField(label=_("Name"), max_length=32, regex=r'[^\n]+$')
    ou_description = forms.RegexField(label=_("Description"), max_length=32, regex=r'[^\n]+$')


    def handle(self, request, data):
        result = salt_utils.create_organizational_unit(request, data['ou_name'], data['ou_description'])
        if result: 
            messages.success(request, 'Organizational unit added successfuly. ')
            return True
        messages.error(request, 'Organizational unit  wasn\'t added. Try again or contact an administrator. ')


#class AddUserToGroupForm(forms.SelfHandlingForm):
#    list_users = forms.MultipleChoiceField(choices=[(1, 'n/a')], label = _("Users not in group"), widget=forms.CheckboxSelectMultiple)
#
#    def __init__(self, request, *args, **kwargs):
#        users = kwargs.pop('samba_users')
#        super(AddUserToGroupForm, self).__init__(self, *args, **kwargs)
#        self.fields['list_users'].choices = [(x['username'], x['username']) for x in users]
#
#    def handle(self, request, data):
#        result = salt_utils.add_user_to_group(request, data['list_users'], request.session['group_name'])
#        if result: 
#            messages.success(request, 'Users added successfuly. ')
#            return True
#        messages.error(request, 'Users weren\'t added. Try again or contact an administrator. ')
