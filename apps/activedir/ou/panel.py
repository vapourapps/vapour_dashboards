import horizon
from ... import dashboard

class OUPanel(horizon.Panel):
    name = "Organizational Units"
    slug = "activedir.ou"

dashboard.Dashboard.register(OUPanel)
