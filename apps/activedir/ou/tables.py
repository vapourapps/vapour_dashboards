from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _


class OUMembersTable(tables.DataTable):
    username = tables.Column('username', verbose_name=_("Username"))
    class Meta:
        name = 'userstable'
        verbose_name = _("Users in Organizational unit")
        table_actions = ()
        row_actions = ()


class ListMembers(tables.LinkAction):
    name = "list_members"
    verbose_name = _("List members")
    url = "horizon:apps:activedir.ou:view_members"
    classes = ("btn-edit", )


class AddOU(tables.LinkAction):
    name = "add_ou"
    verbose_name = _("Add organizational unit")
    url = "horizon:apps:activedir.ou:add"
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True


class OUTable(tables.DataTable):
    name = tables.Column('name', verbose_name=_("Name"))
    description = tables.Column('description', verbose_name=_("Description"))

    class Meta:
        name = "outable"
        verbose_name = _("Organizational Units")
        table_actions = (AddOU, )
        row_actions = (ListMembers, )


