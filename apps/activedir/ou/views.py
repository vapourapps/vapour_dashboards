from horizon import tables, forms, messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import redirect, render
from django.utils.translation import ugettext_lazy as _
from ...utils import *
from . import tables as project_tables
from . import forms as project_forms
from openstack_dashboard.salt_utils import salt_utils
import inspect

#directory_minion='G@role:directory'


class OrganizationalUnit(object):
    def __init__(self, name, description = ''):
        self.id = name
        self.name = name
        if not description: 
            description = ''
        self.description = description

class OUMember(object):
    def __init__(self, name): 
        self.id = name
        self.username = name


class OUMembersView(tables.DataTableView):
    table_class = project_tables.OUMembersTable
    template_name = 'apps/activedir.ou/members.html'

    def get_data(self):
        result = salt_utils.get_ou_members(self.request, self.kwargs['name']) 
        result = [OUMember(username) for username in result]
        print 'Result for OU is : ', result
        return result

class AddOUView(forms.ModalFormView):
    form_class = project_forms.AddOUForm
    template_name = "apps/activedir.ou/add.html"
    success_url = reverse_lazy('horizon:apps:activedir.ou:index')

    def get_context_data(self, **kwargs):
        context = super(AddOUView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {}

class IndexView(tables.DataTableView):
    table_class = project_tables.OUTable
    template_name = 'apps/activedir.ou/ou.html'

    def get_data(self):
        ous = salt_utils.list_organizational_units(self.request)
        result = [OrganizationalUnit(*ou) for ou in ous]
        return result 

#    def get_context_data(self, **kwargs):
#        context = super(IndexView, self).get_context_data(**kwargs)
#        secondary = project_tables.GroupTable(self.request, [Group(name = x) for x in salt_utils.samba_secondary_groups])
#        context['secondary_groups'] = secondary
#        return context  
